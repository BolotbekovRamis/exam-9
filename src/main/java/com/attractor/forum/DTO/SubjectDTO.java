package com.attractor.forum.DTO;

import com.attractor.forum.model.Subject;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubjectDTO {
    private Integer id;

    private String name;
    private String SubjectTime;
    private String customer;
    private int AnswersAmount;

    public static SubjectDTO from(Subject subject) {
        return builder()
                .id(subject.getId())
                .name(subject.getName())
                .SubjectTime(subject.getLdt().toString())
//                .customer(subject.getCustomer())
                .AnswersAmount(subject.getComments().size())
                .build();
    }
}

