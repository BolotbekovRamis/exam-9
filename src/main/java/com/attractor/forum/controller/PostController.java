package com.attractor.forum.controller;

import com.attractor.forum.model.Customer;
import com.attractor.forum.model.Post;
import com.attractor.forum.repository.CustomerRepository;
import com.attractor.forum.repository.PostRepository;
import com.attractor.forum.service.PostService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.time.LocalDate;

@Controller
public class PostController {
    @Autowired
    PostRepository postRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    PostService postService;
    @GetMapping("/createPost")
    public String postCreation(Model model){


        return "post";
    }
    @PostMapping("/createPost")
    public String add(Principal principal, @AuthenticationPrincipal Customer user,
                      @RequestParam String name,
                      @RequestParam String content , Model model) {
        LocalDate localDate =LocalDate.now();
      var authorName= principal.getName();
        Post post = new Post();
        post.setName(name);
        post.setContent(content);
        post.setCreateDate(localDate);
        post.setAuthorName(authorName);
        post.setAuthor(user);
        post.setAuthor(user);
         postRepository.save(post);

        Iterable<Post> posts;
        posts = postRepository.findAll();
        model.addAttribute("posts", posts);


        return "index";
    }
}
