package com.attractor.forum.controller;


import com.attractor.forum.model.Subject;
import com.attractor.forum.service.CustomerService;
import com.attractor.forum.service.SubjectService;
import lombok.var;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SubjectController {
    private final SubjectService themeService;

    public SubjectController(SubjectService SubjectService) {
        this.themeService = SubjectService;
    }

    @GetMapping("/create/theme")
    public String createTheme(Model model){
        return "createTheme";
    }

    @PostMapping("/create/theme")
    public String createTheme(@RequestParam("title") String title, @RequestParam("main_text") String text){
        Subject subject = new Subject();
        subject.setName(title);
        subject.setText(text);
//        subject.setCustomer(CustomerService.findByUsername(getCustomername()));
        themeService.saveTheme(subject);
        return "redirect:/";
    }

    private String getCustomername(){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }
}

