package com.attractor.forum.service;


import com.attractor.forum.DTO.SubjectDTO;
import com.attractor.forum.model.Subject;
import com.attractor.forum.repository.SubjectRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class SubjectService {
    private final SubjectRepository SubjectRepository;

    public void saveTheme(Subject subject){
        SubjectRepository.save(subject);
    }

    public Page<SubjectDTO> getThemes(Pageable pageable){
        return SubjectRepository.findAll(pageable).map(SubjectDTO::from);
    }

    public SubjectDTO getThemeById(Integer themeId){
        return SubjectDTO.from(SubjectRepository.findById(themeId).get());
    }
}
