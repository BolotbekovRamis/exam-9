package com.attractor.forum.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.*;
import java.time.LocalDateTime;

    @Data
    @Table(name = "customer_comments")
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    public class CustomersComments {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;

        @NotBlank
        @Size(min = 1,  max = 500)
        @Column(length = 500)
        private String text;

        @ManyToOne(fetch = FetchType.LAZY)
        private Subject subject;

        @ManyToOne(fetch = FetchType.LAZY)
        private Customer customer;

        @Column(name = "customer_comments_date_time")
        LocalDateTime CommentTime = LocalDateTime.now();
    }