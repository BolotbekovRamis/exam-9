package com.attractor.forum.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import lombok.*;
import java.util.List;


    @Data
    @Table(name = "subjects")
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    public class Subject {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;

        @NotBlank
        @Size(min = 1,  max = 128)
        @Column(length = 128)
        private String name;

        @NotBlank
        @Size(min = 1,  max = 128)
        @Column(length = 128)
        private String text;


        @ManyToOne(fetch = FetchType.LAZY)
        private Customer customer;

        @Column(name = "datetime")
        @Builder.Default
        private LocalDateTime ldt = LocalDateTime.now();

        @OneToMany(mappedBy = "subject")
        List<CustomersComments> comments;
    }
