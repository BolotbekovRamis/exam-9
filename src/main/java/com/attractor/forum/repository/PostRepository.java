package com.attractor.forum.repository;

import com.attractor.forum.model.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post,Integer> {

    Iterable<Post> findAllByName(String search);
}
