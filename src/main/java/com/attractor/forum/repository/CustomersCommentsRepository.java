package com.attractor.forum.repository;

import com.attractor.forum.model.CustomersComments;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomersCommentsRepository extends JpaRepository<CustomersComments, Integer> {

}
