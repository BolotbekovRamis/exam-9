use `forum`;

CREATE TABLE `subjects` (
  `id` INT auto_increment NOT NULL,
  `name` varchar(128) NOT NULL,
  `text` varchar(128) NOT NULL,
  `customer_id` INT NOT NULL,
  `datetime` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_customer` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`));

DROP TABLE `forum`.`food_types`, `forum`.`places`, `forum`.`foods`;