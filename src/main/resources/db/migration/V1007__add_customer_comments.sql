use `forum`;

CREATE TABLE `customer_comments` (
      `id` INT auto_increment NOT NULL,
      `text` varchar(500) NOT NULL,
      `subject_id` INT NOT NULL,
      `customer_id` INT NOT NULL,
      `customer_comments_date_time` DATETIME NOT NULL,
      PRIMARY KEY (`id`),
      CONSTRAINT `FK_SubjectComment` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`),
      CONSTRAINT `FK_CustomerComments` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`));